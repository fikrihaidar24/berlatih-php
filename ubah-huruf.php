<?php
function ubah_huruf($string){
//kode di sini
$arr = array("w" => "x","o" => "p","d" => "e","e"=>"f","v" => "w","l"=>"m","p"=>"o","a" => "b","k"=>"l","n"=>"o","m"=>"n","g"=>"h","t"=>"u","s"=>"t","r"=>"s","p" => "q");
echo strtr($string ,$arr);
echo "<br>";
}

// TEST CASES
echo ubah_huruf('wow'); // xpx
echo ubah_huruf('developer'); // efwfmpqfs
echo ubah_huruf('laravel'); // mbsbwfm
echo ubah_huruf('keren'); // lfsfo
echo ubah_huruf('semangat'); // tfnbohbu

?>